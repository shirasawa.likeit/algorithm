package algorithm.For;

public class Question1 {

	public static void main(String[] args) {
		/* TODO 以下の３角形作る
		*	■
		*	■ ■
		*	■ ■ ■
		*	■ ■ ■ ■
		*   ■ ■ ■ ■ ■
		*/

		
		//縦軸 5行
		for(int i=1;i<6;i++) {
			
			//横軸 1~5列
			for(int j=1;j<=i;j++) {
				System.out.print("■");
			}
			//改行
			System.out.println();
		}

	}

}
