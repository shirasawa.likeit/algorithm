package algorithm.For;

public class Question3 {

	public static void main(String[] args) {
		/* TODO 以下の九九の表を作成
		
		  |  1  2  3  4  5  6  7  8  9
		-----------------------------------------
		1 |  1  2  3  4  5  6  7  8  9
		2 |  2  4  6  8 10 12 14 16 18
		3 |  3  6  9 12 15 18 21 24 27
		4 |  4  8 12 16 20 24 28 32 36
		5 |  5 10 15 20 25 30 35 40 45
		6 |  6 12 18 24 30 36 42 48 54
		7 |  7 14 21 28 35 42 49 56 63
		8 |  8 16 24 32 40 48 56 64 72
		9 |  9 18 27 36 40 54 63 72 81
		*/

		//ヘッダー

		System.out.print("  |");
		for (int i = 1; i < 10; i++) {
			System.out.print("  " + i);
		}
		//改行
		System.out.println();
		//区切り線
		System.out.println("-----------------------------------------");

		//縦軸
		for (int j = 1; j < 10; j++) {
			System.out.print(j + " |");
			//横軸
			for (int k = 1; k < 10; k++) {

				//掛け算
				int num = j * k;

				//計算結果が10以下(1桁)だったら空白をプラス
				if (num < 10) {
					System.out.print(" ");
				}
				//計算結果出力
				System.out.print(" " + num);
			}
			//改行
			System.out.println();
		}
	}

}
