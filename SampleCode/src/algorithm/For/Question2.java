package algorithm.For;

public class Question2 {

	public static void main(String[] args) {
		/* TODO 以下の3角形を作る
		             ■
		          ■ ■		
		       ■ ■ ■
		    ■ ■ ■ ■
		 ■ ■ ■ ■ ■ 
		 */

		for (int i = 5; i > 0; i--) {
			//空白を出力した数をカウント
			int count = 0;
			//空白部分
			for (int j = 1; j < i; j++) {
				System.out.print("  ");
				count++;
			}
			//■部分
			for (int k = 1; k <= 5 - count; k++) {
				System.out.print("■");
			}
			//改行
			System.out.println();
		}

	}

}
