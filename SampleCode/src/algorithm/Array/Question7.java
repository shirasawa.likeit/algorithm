package algorithm.Array;

public class Question7 {

	public static void main(String[] args) {
		/* TODO 配列に「1, 3, 5, 7, 8, 12, 14, 20 」を格納し、平均を出力。
		 * また平均以下の数値も出力。 
		 */

		//合計を格納する変数
		int total = 0;

		//平均を格納する変数
		int average = 0;

		//配列を作成
		int[] array = { 1, 3, 5, 7, 8, 12, 14, 20 };

		//配列内の合計
		for (int num : array) {
			total += num;
		}

		//平均を求める
		average = total / array.length;

		//平均値を出力
		System.out.println("平均：" + average);

		//平均値以下の数値を出力
		for (int num : array) {
			if (num < average) {
				System.out.print(num + " ");
			}
		}

	}

}
