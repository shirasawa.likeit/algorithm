package algorithm.Array;

public class Question6 {

	public static void main(String[] args) {
		/* TODO 5で作成した配列を使用し以下を出力する。
		 * ・要素番号0～9の間の奇数を出力
		 * ・要素番号10～19の間の偶数を出力 
		 */

		//配列作成(要素数20個)
		int[] array = new int[20];

		//配列内に5の倍数を格納
		for (int i = 0; i < array.length; i++) {
			array[i] = 5 * (i + 1);
		}

		//要素番号0~9の間の奇数を出力
		for (int j = 0; j <= 9; j++) {

			if (array[j] % 2 == 1) {
				System.out.println(array[j]);
			}

		}

		//要素番号10~19の間の偶数を出力
		for (int k = 10; k < array.length; k++) {

			if (array[k] % 2 == 0) {
				System.out.println(array[k]);
			}

		}

	}

}
