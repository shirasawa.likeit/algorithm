package algorithm.Array;

public class Question8 {

	public static void main(String[] args) {
		/*
		 *  TODO 配列に「5, 8, 1, 9, 3, 6, 4 」を格納し、
		 * 配列内の2番目に大きい数字を出力する。
		 * ※バブルソートを使って解く
		 *   動きがわからない場合はデバッグするとわかりやすい！
		 *
		 */
		
		

		//配列を作成(要素数7個)
		int[] array = { 5, 8, 1, 9, 3, 6, 4 };
		
		//配列内の数字を小さい順に並べる
		for (int i = array.length - 1; i > 0; i--) {

			for (int j = 0; j < i; j++) {

				//隣同士を比べる
				if (array[j] > array[j + 1]) {
					//値の入れ替え
					int tmp = array[j + 1];
					array[j + 1] = array[j];
					array[j] = tmp;
				}
			}
		}
		
		//配列の要素後ろから2番目を出力(小さい順に並んでいるため)
		System.out.println(array[array.length-2]);
	}

}
