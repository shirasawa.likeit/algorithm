package algorithm.Array;

public class Question4 {

	public static void main(String[] args) {
		/* TODO Question3で作成したコードを使用し以下を出力する。
		 * 偶数は〇〇個です。
		 * 合計は〇〇です。(配列内の合計)
		 */

		int count = 0;
		int sum = 0;

		//配列作成
		int[] array = new int[10];

		//数値格納
		for (int i = 0; i < array.length; i++) {
			array[i] = i + 1;
		}

		for (int ar : array) {

			//配列内の偶数の数カウント
			if (ar % 2 == 0) {
				count++;
			}
		}
		//偶数の数を出力
		System.out.println("偶数は" + count + "個です");

		for (int ar : array) {

			//配列内の合計を計算
			sum += ar;
		}

		//合計を出力
		System.out.println("合計は" + sum + "です");

	}

}
