package algorithm.Array;

public class Question2 {

	public static void main(String[] args) {
		/* TODO 要素数が20個の配列を作成し、配列の要素の0~9までの間に入っている偶数、
		 * 10~19までの間に入っている奇数を出力。
		 * (格納する数は1～20までの数字)
		 */

		//配列を作成(要素数20個)
		int[] array = new int[20];

		//配列に数字を格納(1~20)
		for (int i = 0; i < array.length; i++) {

			array[i] = i + 1;

		}

		//0~9までくりかえし
		for (int i = 0; i <= 9; i++) {

			//偶数判定
			if (array[i] % 2 == 0) {
				System.out.print(array[i] + " ");
			}

		}

		//改行
		System.out.println();

		//10~19までくりかえし
		for (int i = 10; i < array.length; i++) {

			//奇数判定
			if (array[i] % 2 == 1) {
				System.out.print(array[i] + " ");
			}

		}

	}

}
