package algorithm.Array;

public class Question1 {

	public static void main(String[] args) {
		/* TODO 配列に1～10までの数が格納された配列を作成。
		 * 		配列を偶数、奇数で出力
		 *		条件：カンマ区切りにする。ただし最後の数字にはカンマをつけない。
		 */

		//配列を作成(要素数10個)
		int[] array = new int[10];

		//配列に数字を格納(1~10)
		for (int i = 0; i < array.length; i++) {

			array[i] = i + 1;

		}

		//if文を通過した数をカウントする変数
		int count = 0;

		//偶数判定
		for (int i = 0; i < array.length; i++) {

			if (array[i] % 2 == 0) {

				//if文を通過した回数カウント
				count++;
				
				//カンマをつける
				if (count != 1) {
					System.out.print(",");
				}

				//偶数を出力
				System.out.print(array[i]);

			}
		}

		//改行
		System.out.println();

		//count初期化
		count = 0;

		//奇数判定
		for (int i = 0; i < array.length; i++) {

			if (array[i] % 2 == 1) {

				//if文を通過した回数カウント
				count++;

				//カンマをつける
				if (count != 1) {
					System.out.print(",");
				}

				//奇数を出力
				System.out.print(array[i]);
			}

		}
	}

}
