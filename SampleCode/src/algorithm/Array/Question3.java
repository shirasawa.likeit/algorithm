package algorithm.Array;

public class Question3 {

	public static void main(String[] args) {
		// TODO 配列に1～10まで格納し、2の倍数を出力する

		int[] array = new int[10];

		for (int i = 0; i < array.length; i++) {
			array[i] = i + 1;
		}

		for (int ar : array) {

			if (ar % 2 == 0) {
				System.out.print(ar + " ");
			}
		}
	}

}
