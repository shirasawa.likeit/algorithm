package algorithm.Array;

public class Question5 {
	public static void main(String[] args) {
		// TODO 要素数20個の配列に5の倍数の数字を順番に入れ、すべて出力する。

		//配列作成(要素数20個)
		int[] array = new int[20];

		//配列内に5の倍数を格納
		for (int i = 0; i < array.length; i++) {
			array[i] = 5 * (i + 1);
		}

		//配列を出力
		for (int num : array) {
			System.out.println(num);
		}

	}
}
